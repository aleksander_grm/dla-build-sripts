# Build scripts for DLAF and DLAI projects

Download or clone scripts to your location with

git clone https://gitlab.com/aleksander_grm/dla-build-sripts.git

In the directory you find three folders:
* dlaf - contains DLA-Future building scripts
* dlai - contains DLA-Interface building scripts
* spack - spack installation environment for HPC@UL-FS 

------------------
**Spack installation**

First, you need to install SPACK environment. Run spack_install.sh script that will install all the necessary files.

To include spack for later use, you need to source spack environment. Put line

```
source /opt/spack/share/spack/setup-env.sh
```

in your .bashrc file to start spack when you login to the system.
 
------------------
**GCC environment**

Must set proper gcc version. Check, which compiler exists

```
spack compiler find
```

Try to load GCC 10.3.0 if not present, install it

```
spack install gcc@10.3.0
```

Before compiling some package, you need to laod it first

```
spack load gcc@10.3.0
```



------------------

**DLA-Future** library:

First must build DLA-Future library:
1. cd into dlaf
2. run build_dlaf.sh
3. all should be done automatically (compile and tests)

dlaf folder contains build_miniapp.sh and run_miniapp.sh script to test DLA-Future library. It is an example of how to use DLA-Future library stand alone.

1. run build_miniapp.sh to build miniapp
2. to test execute

```
$> run_miniapp.sh
```


-----------

**DLA-Interface** library:

Second must build DLA-Interface library:
1. cd into dlai
2. run build_dlai.sh
3. all should be done automatically (compile and tests)

dlai folder contains run_hpc.sh script to test Cholesky factorization with different parameters

```
$> run_hpc.sh N_TRD N_SIZE N_BLOCK
```

where:
* N_TRD is the number of threads
* N_SIZE is the matrix size (multiple of 1028 -> for example 10280)
* N_BLOCK is the block size (multiple of 128 -> for example 256)

Example:
```
$> run_hpc.sh 12 10280 256
```


