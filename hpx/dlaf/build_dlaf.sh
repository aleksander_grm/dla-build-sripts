#!/bin/bash
#
# author: aleksander.grm@fpp.uni-lj.si
# date: 01/10/2021

# ******************************************************************
# *** Install & test DLA-Future system locally in specified path ***
# ******************************************************************

# Now use SPACK technology to simplify build of a system
rpath=$(pwd)

# set project git branch
DLAF_VER=dlaf-master
rm -rf ${rpath}/${DLAF_VER}
git clone https://gitlab.com/cscs-ci/eth-cscs/DLA-Future.git ${DLAF_VER}

# set paths
DLAF_SRC_PATH=${rpath}/${DLAF_VER}
cd ${DLAF_SRC_PATH}
export DLAF_ROOT=${DLAF_SRC_PATH}

# *** build and compile DLA-Future ***

# clean dlaf spack repo if exists
spack repo rm ${DLAF_SRC_PATH}/spack 

# load proper gcc version (!!! before needs to be installed if not !!! spack install gcc@10.3.0)
spack load gcc@10.3.0 

# adds new repo
spack repo add $DLAF_ROOT/spack 

# build & install dlaf with specific gcc version
spack install dla-future@hpx_latest %gcc@11.2.0 ^intel-mkl ^hpx@1.7.0
