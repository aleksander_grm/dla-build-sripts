#!/bin/bash -e

echo "Loading modules ..."
spack load cmake@3.21.1
spack load gcc@10.3.0
spack load dla-future@develop

echo "Copy example ..."
export DLAF_INSTALL_PREFIX=$(spack location -i dla-future)

MINIAPP_SRC_PATH=$(pwd)/dlaf-master/miniapp
MINIAPP_DST_PATH=$(pwd)/miniapp

# Copy fresh source to destination directory
cp -rf ${MINIAPP_SRC_PATH}/miniapp_cholesky.cpp ${MINIAPP_DST_PATH}

echo "Building ..."
# build
cd ${MINIAPP_DST_PATH}
cmake ${MINIAPP_DST_PATH} \
	-DCMAKE_INSTALL_PREFIX=${DLAF_INSTALL_PREFIX}

# compile
make
