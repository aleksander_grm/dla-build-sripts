#
# Distributed Linear Algebra with Future (DLAF)
#
# Copyright (c) 2018-2021, ETH Zurich
# All rights reserved.
#
# Please, refer to the LICENSE file in the root directory.
# SPDX-License-Identifier: BSD-3-Clause
#
cmake_minimum_required(VERSION 3.14)
project (MINIAPP_DLAF)

find_package(LAPACK REQUIRED)
find_package(DLAF REQUIRED)
find_package(Umpire REQUIRED)

add_executable(miniapp_cholesky miniapp_cholesky.cpp)
target_link_libraries(miniapp_cholesky PRIVATE DLAF::DLAF)
