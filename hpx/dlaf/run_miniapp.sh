#!/bin/bash -e

echo "Loading modules ..."
spack load dla-future@develop

MINIAPP_SRC_PATH=$(pwd)/miniapp

# run test
echo "Running miniapp default ..."
${MINIAPP_SRC_PATH}/miniapp_cholesky

# Run with parameters
N_TRD=12
M_SIZE=2048
B_SIZE=512
echo "Running miniapp: N_TRD=${N_TRD}, M_SIZE=${M_SIZE}, B_SIZE=${B_SIZE} ..."
${MINIAPP_SRC_PATH}/miniapp_cholesky --hpx:threads=${N_TRD} --matrix-size=${M_SIZE} --block-size=${B_SIZE} \ 
