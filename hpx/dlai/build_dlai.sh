#!/bin/bash
#
# author: aleksander.grm@fpp.uni-lj.si
# date: 01/10/2021

# *********************************************************************
# *** Install & test DLA-Interface system locally in specified path ***
# *********************************************************************

#
# *** Parameters ***
#
rpath=$(pwd) # set local path for the installation
nproc=32     # number of processors for parallel system build

# ******************************************
# *** HPC-FS Needed modules to be loaded ***
# ******************************************
#
# ***************************************************
# !!! User MUST specify its own packages versions !!!
# ***************************************************
#
GCC_VER=gcc@11.2.0
echo "Loading modules ..."
spack load ${GCC_VER}
spack load git@2.31.1%${GCC_VER}
spack load cmake@3.22.2%${GCC_VER}
spack load openmpi@4.1.2%${GCC_VER}
spack load intel-mkl@2020.4.304%${GCC_VER}
spack load umpire@5.0.1%${GCC_VER}
spack load googletest@1.10.0%${GCC_VER}


# *****************************
# *** Build with DLA-Future ***
# *****************************

# uncomment to build with DLA-Future support
build_with_DLAF=true

if [ ! -z ${build_with_DLAF} ]; then
	spack load dla-future@hpx_latest%${GCC_VER}

	BLASPP_PATH=$(spack location -i blaspp%${GCC_VER})
	LAPACKPP_PATH=$(spack location -i lapackpp%${GCC_VER})
	HPX_PATH=$(spack location -i hpx%${GCC_VER})
	DLAF_PATH=$(spack location -i dla-future@hpx_latest%${GCC_VER})
	
	echo "BLASPP_PATH: ${BLASPP_PATH}"
	echo "LAPACKPP_PATH: ${LAPACKPP_PATH}"
	echo "HPX_PATH: ${HPX_PATH}"
	echo "DLAF_PATH: ${DLAF_PATH}"
	
	export blaspp_DIR=${BLASPP_PATH}
	export lapackpp_DIR=${LAPACKPP_PATH}
	export HPX_DIR=${HPX_PATH}
else
	echo "*** Building withouth DLA-Future support ***"
fi

# *****************************


echo
echo "*** DLA environment system test ***"
echo

# Clean previous installations
#
echo
echo " *** Cleaning old instalations ..."
./clean_all.sh
read -t 2 -p " Done with cleaning!"
echo

# *** Build DLA-Interface
echo
read -t 2 -p " Starting with DLA-Interface ..."
echo
cd ${rpath}
./install_dlai.sh ${rpath} ${nproc} ${build_with_DLAF} ${DLAF_PATH}
echo
read -t 2 -p " Done with DLA-Interface!"
echo
