#!/bin/bash

# Install DLA-Interface
rpath=$1
nproc=$2
build_with_DLAF=$3

echo "rpath: ${rpath}"

# 
# *** Obtain DLA-Interface version ***
#

# - set the correct branch
#DLAI_VER=master                                                                                                 
DLAI_VER=feature/dlaf-integration-hpx

DLAI_PATH=${rpath}/dlai
DLAI_SRC_PATH=${DLAI_PATH}-${DLAI_VER}
rm -rf ${DLAI_PATH}
rm -rf ${DLAI_SRC_PATH}

if [ "${DLAI_VER}" = "master" ]; then
	echo "Cloning master ..."
	git clone https://gitlab.com/cscs-ci/eth-cscs/DLA-interface.git dlai-${DLAI_VER}
else
	echo "Cloning branch ${DLAI_VER}"
	git clone --single-branch --branch ${DLAI_VER} https://gitlab.com/cscs-ci/eth-cscs/DLA-interface.git dlai-${DLAI_VER}
fi

#
# CMake file needs correction. should be done in new version!
#
# MPI include error; added line 62 - "include_directories(${MPI_INCLUDE_PATH})"
# Umpire error; adde line 65 - "find_package(Umpire REQUIRED)"
echo "*** copy corrected CMakeLists.txt file"
cp -f CMakeLists.txt ${DLAI_SRC_PATH}

BUILD_PATH=${DLAI_SRC_PATH}/build
cd ${DLAI_SRC_PATH}
if [ -d ${BUILD_PATH} ]; then
    rm -rf ${BUILD_PATH}
fi
mkdir ${BUILD_PATH}
cd ${BUILD_PATH}

echo
echo "Build path: ${BUILD_PATH}"
echo "Install path: ${DLAI_PATH}"
echo

# Build
if [ ! -z ${build_with_DLAF} ]; then
    cmake ${DLAI_SRC_PATH} \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo \
        -DDLAI_WITH_MKL=ON \
        -DMKL_SCALAPACK_TARGET="mkl::scalapack_ompi_intel_32bit_seq_dyn" \
        -DDLAI_WITH_DLAF=ON \
        -DDLAF_ROOT=${DLAF_PATH} \
        -DCMAKE_INSTALL_PREFIX=${DLAI_PATH}
else
    cmake ${DLAI_SRC_PATH} \
        -DCMAKE_BUILD_TYPE=RelWithDebInfo \
        -DDLAI_WITH_MKL=ON \
        -DMKL_SCALAPACK_TARGET="mkl::scalapack_ompi_intel_32bit_seq_dyn" \
        -DDLAI_WITH_DLAF=OFF \
        -DDLAF_ROOT=${DLAF_PATH} \
        -DCMAKE_INSTALL_PREFIX=${DLAI_PATH}
fi

# Compile
# make VERBOSE=1 -j${nproc}
make -j${nproc}

# Install
make install

# Test
ctest

