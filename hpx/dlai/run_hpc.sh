#!/bin/bash
#SBATCH --partition=haswell
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=miniapp_test

rpath=$(pwd)

# number of threads per node
N_TRD=$1  # first argument, number of threads
M_SIZE=$2 # second argument, matrix size (multiple of 1028 -> size=10280)
B_SIZE=$3 # third argument, block size (multiple of 128 -> size=256)

# check if set, otherwise set defaults
if [ -z ${N_TRD} ]; then 
	N_TRD=16
fi
if [ -z ${M_SIZE} ]; then
    M_SIZE=10280
fi
if [ -z ${B_SIZE} ]; then
    B_SIZE=256
fi

#
# *** HPC-FS Needed modules to be loaded ***
#
GCC_VER=gcc@11.2.0
echo "Loading modules ..."
spack load openmpi@4.1.2%${GCC_VER}
spack load intel-mkl@2020.4.304%${GCC_VER}
spack load umpire@5.0.1%${GCC_VER}

# set correct flag
#build_with_DLAF=ON

if [ ! -z ${build_with_DLAF} ]; then
    
    spack load dla-future@hpx_latest%${GCC_VER}
    
    BLASPP_PATH=$(spack location -i blaspp)
    LAPACKPP_PATH=$(spack location -i lapackpp)
    HPX_PATH=$(spack location -i hpx)
    DLAF_PATH=$(spack location -i dla-future)

    echo "BLASPP_PATH: ${BLASPP_PATH}"
    echo "LAPACKPP_PATH: ${LAPACKPP_PATH}"
    echo "HPX_PATH: ${HPX_PATH}"
    echo "DLAF_PATH: ${DLAF_PATH}"

    export blaspp_DIR=${BLASPP_PATH}
    export lapackpp_DIR=${LAPACKPP_PATH}
    export HPX_DIR=${HPX_PATH}
fi

${rpath}/run_miniapp.sh ${N_TRD} ${M_SIZE} ${B_SIZE} ${rpath} ${build_with_DLAF}
