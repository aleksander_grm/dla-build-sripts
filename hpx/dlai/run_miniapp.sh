#!/bin/bash

# set parameters to run mimiapp aplication
N_TRD=$1  
M_SIZE=$2
B_SIZE=$3
rpath=$4
build_with_DLAF=$5

# Test usage
# ./run_miniapp.sh 16 10280 256 $(pwd) 1

echo
echo "Running miniapp with the following option set:"
echo "   -> number of threads: ${N_TRD}"
echo "   -> matrix size: ${M_SIZE}"
echo "   -> block size: ${B_SIZE}"

if [ ! -z ${build_with_DLAF} ]; then
    echo "   -> including test with DLA-Future"
    DLAI_VER=feature/dlaf-integration-hpx
    
    GCC_VER=gcc@11.2.0
    echo "Loading modules ..."
    spack load openmpi@4.1.2%${GCC_VER}
    spack load intel-mkl@2020.4.304%${GCC_VER}
    spack load umpire@5.0.1%${GCC_VER}
    spack load dla-future@hpx_latest%${GCC_VER}
else
    echo "   -> including test without DLA-Future"
    DLAI_VER=master
fi

echo "   -> started ..."
echo                                                                                                   

MINI_APP=${rpath}/dlai-${DLAI_VER}

export OMP_NUM_THREADS=1

# test with DLAF
if [ ! -z ${build_with_DLAF} ]; then
    echo
    echo "Testing with DLAF ..."
    echo
    ${MINI_APP}/build/miniapp/overlap_gaussian_orbitals --dlaf --nr_threads ${N_TRD} --n ${M_SIZE} --nb ${B_SIZE}
fi

# test with ScaLAPACK
echo
echo "Testing with ScaLAPACK ..."
echo
${MINI_APP}/build/miniapp/overlap_gaussian_orbitals --scalapack --nr_threads ${N_TRD} --n ${M_SIZE} --nb ${B_SIZE}

