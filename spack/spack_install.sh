#!/bin/bash

rmdir ~/.spack
mkdir ~/.spack
sed -e '/root:/s,$spack,/work/$USER,' /opt/spack/etc/spack/defaults/config.yaml  > ~/.spack/config.yaml
sed -i -e '/source_cache:/s,:.*,: ~/.spack/cache,' ~/.spack/config.yaml
sed -i -e 's/# build_jobs: 16/build_jobs: 32/' ~/.spack/config.yaml
sed -e 's/# roots:/roots:/' -e 's,#  lmod:.*,  lmod: /work/$USER/opt/lmod,' -e 's,#  tcl:.*,  tcl: /work/$USER/opt/modules,' -e '/- tcl/a\      - lmod' /opt/spack/etc/spack/defaults/modules.yaml > ~/.spack/modules.yaml

# Run source in active bash
# Add source line in your .bashrc profile file
#source /opt/spack/share/spack/setup-env.sh
